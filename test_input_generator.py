from random import shuffle
class Generator:
    def _init_(self):
        return

    def get_data(self):
        return {
                "names": [
                    "Dawit", "Gizaw", "Hassan", "Olana", "Kasu", "Mattie", "Elshaday", "Elsa", "Firaol", "Durmamu"
                    ],
                "address": [
                    "Arat Kilo", "Saris, Abo", "Megenagna", "Gedel Gibu", "22 Mazoriya"
                    ],
                "ages": [x for x in range(13, 60, 2)]
                }

    def get_names(self, amount):
        names = self.get_data()["names"]
        while amount > len(names):
            names.extend(names)
        shuffle(names)
        return names[0:amount]

    def get_full_name(self, amount):
        for fname, lname in zip(self.get_names(amount), self.get_names(amount)):
            yield(fname + " " + lname)

    def get_address(self, amount):
        addresses = self.get_data()["address"]
        while amount > len(addresses):
            addresses.extend(addresses)
        shuffle(addresses)
        return addresses[0:amount]

    def get_ages(self, amount):
        ages = self.get_data()["ages"]
        while amount > len(ages):
            ages.extend(ages)
        shuffle(ages)
        return ages[0:amount]







