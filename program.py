#!usr/bin/python

from test_input_generator import Generator

class Registration():
    users = []
    def _init_(self):
        return

    def register(self, data):
       self.users.insert(0, {
            "name": data["name"],
            "lname": data["lname"],
            "address": data["address"],
            "age": data["age"]
            })
       return self.users

    def get_users(self):
        return self.users



class Test():
    registration = Registration()

    def _init_(self):
        return

    def do_registration(self, data):
        initial = len(self.registration.get_users())
       # self.registration.register({"name": "Mbaku", "lname": "Jabari", "address": "Jabari", "age": 20})
        self.registration.register(data)
        assert((len(self.registration.get_users()) - initial) == 1)
        print "registration success"
        return

test = Test()
gen = Generator()


test.do_registration({"name": gen.get_names(1), "lname": gen.get_names(1), "address": gen.get_address(1), "age": gen.get_ages(1)})









