# Test Input Generator

_Name_:		**Henok Million** 
_ID_:		**ATR/1810/08**
_Department_:	**SE - 2 (R)**

#### Running the program
To begin the test run `$ python program.py`.
It should print `registration success` if the test passes

## Java Branch
The java branch contains a similar test case written in 
Java

